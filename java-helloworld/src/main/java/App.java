import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class App {

  public static void main(String[] args) {

    //variables definitions

    //using classes
    String text = "abc";
    Integer smallNum = 10;
    Long bigNum = 100000000L;
    Float preciseNum = 10.5F;
    List<String> listOfStrings = new ArrayList<>();
    Boolean isEnabled = true;
    Character c = 'F';
    Double preciseNum2 = 234324.44D;

    //using primitive types
    var myNum = 5;
    long myLong = 10000001L;
    var myFloatNum = 5.99f;
    char myLetter = 'D';
    boolean myBool = true;
    double myDouble = 4.5D;
    var text2 = "abc";


    //conditionals

    if (text.equals("abc")) {
      System.out.println("if1");
    } else if (text.equals("cba")) {
      System.out.println("if2");
    } else {
      System.out.println("if3");
    }

    //loops

    //foreach
    var list1 = List.of("a", "b", "c", "d");
    for (String element : list1) {
      System.out.println(element);
    }

    //for
    for (var i = 0; i < 10; i++) {
      System.out.println("for-" + i);
    }

    //while
    int v = 10;
    while (v > 0) {
      System.out.println("while-" + v);
      v--;
    }

    //stream
    var list2 = List.of("a", "b", "c", "d");
    var list2mapped = list2.stream()
        .map(s -> s + "1")
        .filter(s -> !s.equals("b1"))
        .collect(Collectors.toList());
    System.out.println(list2mapped);

    //custom class
    var person = new Person("Kuba", 32);
    System.out.println(person);
    person.increaseAge(10);
    System.out.println(person);
  }
}
