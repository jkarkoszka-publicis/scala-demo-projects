class Person(var name: String, var age: Int) {

  def increaseAge(years: Int): Unit = {
    age += years
  }

  override def toString: String = {
    "name = " + name + ", age = " + age
  }
}
