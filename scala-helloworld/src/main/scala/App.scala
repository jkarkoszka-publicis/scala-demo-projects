
object App {

  def main(args: Array[String]): Unit = {

    //variables definitions

    //using type declaration
    var text: String = "abc"
    val smallNum: Int = 10
    val bigNum: Long = 100000000L
    val preciseNum: Float = 10.5F
    val listOfStrings: List[String] = List("b", "c")
    val isEnabled: Boolean = true
    val c: Char = 'F'
    val preciseNum2: Double = 234324.44D
    val person1 = new Person("Kuba", 33)
    val address = Address("line1", "line2")

    //without type declaration (results are the same)
    val myNum = 5
    val myLong = 1000001L
    val myFloatNum = 5.99f
    val myLetter = 'D'
    val myBool = true
    val myDouble = 4.5D
    val text2 = "abc"

    //conditionals - same as in Java
    //you can use Java classes like: System.out.println
    if (text.equals("abc")) {
      println("if1")
    } else if (text.equals("cba")) {
      println("if2")
    } else {
      System.out.println("if3")
    }

    //loops
    //foreach
    val list1 = List("a", "b", "c", "d")
    for (element <- list1) {
      println(element)
    }

    //for
    for (i <- 0 until 10) {
      println("for-" + i)
    }

    //for on map
    val ratings = Map(
      "Lady in the Water"  -> 3.0,
      "Snakes on a Plane"  -> 4.0,
      "You, Me and Dupree" -> 3.5
    )
    for ((name,rating) <- ratings) {
      println(s"Movie: $name, Rating: $rating")
    }

    //while
    var v = 10
    while (v > 0) {
      println("while-" + v)
      v -= 1
    }

    //stream
    val list2 = List("a", "b", "c", "d")
    val list2mapped = list2 //         var list2mapped = list2.stream()
      .map(v => v + "1") //               .map(s -> s + "1")
      .filter(v => !v.equals("b1"))
      //    .filter(s -> !s.equals("b1"))
      //                                  .collect(Collectors.toList);
   println(list2mapped)


    //custom class
    val person = new Person("Kuba", 32)
    println(person)
    person.increaseAge(10)
    println(person)

  }
}
