case class Address(var line1: String, var line2: String) {

  def changeLine2(line2: String): Unit = {
    this.line2 = line2
  }

  override def toString: String = {
    "line1 = " + line1 + ", line2 = " + line2
  }
}
