ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.12.10"

ThisBuild / libraryDependencies += "org.apache.spark" %% "spark-core" % "3.3.0"
ThisBuild / libraryDependencies += "org.apache.spark" %% "spark-sql" % "3.3.0"
ThisBuild / libraryDependencies += "org.apache.hadoop" % "hadoop-common" % "3.3.3"
ThisBuild / libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "3.3.3"
ThisBuild / libraryDependencies += "org.apache.hadoop" % "hadoop-aws" % "3.3.3"
ThisBuild / libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.13" % "test"

lazy val root = (project in file("."))
  .settings(
    name := "HelloWorld3"
  )

