import org.scalatest.flatspec.AnyFlatSpec

class PersonSpec extends AnyFlatSpec {

  "A person" should " have correct state when given correct value" in {
    //given
    val expectedId = 1
    val expectedGender = "male"
    val age = 20L
    val person = new Person(expectedId, expectedGender, age)
    val expectedAge = 21L;

    //when
    val newPerson = person.increaseYear(1)

    //then
    assert(newPerson.age === expectedAge)
  }
}
