import org.apache.spark._

object HelloWorldLocalFile {

  def parsLine(line: String) = {

    val fields = line.split(",")

    val gender = fields(1).toString
    val age = fields(2).toInt

    (gender, age)
  }

  def main(args: Array[String]): Unit = {
    val sparkContext = new SparkContext("local[*]", "GenderWithAverageAge")
    val finalRdd = sparkContext
      .textFile("peopleAndAge_small.csv")
      .map(parsLine)
      .mapValues(x => (x, 1))
      .reduceByKey((x, y) => (x._1 + y._1, x._2 + y._2))
      .mapValues(x => x._1 / x._2)
    finalRdd.collect().sorted.foreach(println)
  }
}
