import org.apache.spark.sql.SparkSession

object HelloWorldLocalFileDataFrame {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("GenderWithAverageAge")
      .getOrCreate();

    import spark.implicits._

    val finalDf = spark.read.format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .load("peopleAndAge_small_with_header.csv")
      .select("gender", "age")
      // .where("gender == 'male'")
      .groupBy("gender")
      .avg("age")

    finalDf
      .write
      .format("csv")
      .save("results")

    finalDf.show()
  }
}
