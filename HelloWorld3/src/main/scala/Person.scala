case class Person(id: Long,
                  gender: String,
                  age: Long) {
  def increaseYear(n: Long): Person = {
    Person(id, gender, age + n);
  }
}
