import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}
import org.apache.spark.sql.{Row, SparkSession}

object HelloWorldLocalFileDataFrameMapping {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("GenderWithAverageAge")
      .getOrCreate();

    val customSchema = StructType(Array(
      StructField("id", LongType, false),
      StructField("gender", StringType, false),
      StructField("age", LongType, false))
    )

    val encoder = RowEncoder(customSchema)

    val finalDf = spark.read.format("csv")
      .option("header", "false")
      .schema(customSchema)
      .load("peopleAndAge_small.csv")
      .map { row =>
        Row(row.getAs[Long]("id"),
          row.getAs[String]("gender"),
          row.getAs[Long]("age") + 10,
        )
      }(encoder)
      .select("gender", "age")
      .groupBy("gender")
      .avg("age")

    finalDf.show()
  }
}
