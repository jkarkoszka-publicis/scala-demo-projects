import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col

object HelloWorldLocalFileDataFrameMapping2 {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("GenderWithAverageAge")
      .getOrCreate();

    val finalDf = spark.read.format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .load("peopleAndAge_small_with_header.csv")
      .withColumn("age", col("age").plus(20))
      .select("gender", "age")
      .groupBy("gender")
      .avg("age")

    finalDf.show()
  }
}
