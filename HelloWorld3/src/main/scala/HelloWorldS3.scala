import org.apache.log4j._
import org.apache.spark.sql.SparkSession

object HelloWorldS3 {

  def parsLine(line: String) = {

    val fields = line.split(",")

    val gender = fields(1).toString
    val age = fields(2).toInt

    (gender, age)
  }

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel((Level.ERROR))

    val spark: SparkSession = SparkSession.builder()
      .master("local[1]")
      .appName("SparkByExamples.com")
      .getOrCreate()
    spark.sparkContext
      .hadoopConfiguration.set("fs.s3a.access.key", "AKIAYQ52KB2IPAQM3DB6")
    spark.sparkContext
      .hadoopConfiguration.set("fs.s3a.secret.key", "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")
    spark.sparkContext
      .hadoopConfiguration.set("fs.s3a.endpoint", "s3.amazonaws.com")
    spark.sparkContext.setLogLevel("ERROR")

    val results = spark.sparkContext.textFile("s3a://spark-bucket-23409823/peopleAndAge.csv")
      .map(parsLine)
      .mapValues(x => (x, 1))
      .reduceByKey((x, y) => (x._1 + y._1, x._2 + y._2))
      .mapValues(x => x._1 / x._2)
      .collect()

    results.sorted.foreach(println)
  }
}
