import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}

object HelloWorldLocalFileDataFrameNoHeader {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("GenderWithAverageAge")
      .getOrCreate();

    val customSchema = StructType(Array(
      StructField("id", LongType, false),
      StructField("gender", StringType, false),
      StructField("age", LongType, false))
    )

    spark.read.format("csv")
      .option("header", "false")
      .schema(customSchema)
      .load("peopleAndAge_small.csv")
      .select("gender",  "age")
      .groupBy("gender")
      .avg("age")
      .show()
  }
}
