import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.avg

object HelloWorldLocalFileDataSet {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("GenderWithAverageAge")
      .getOrCreate();
    import spark.implicits._

    spark.read.format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .load("peopleAndAge_small_with_header.csv").as[Person]
      .groupBy("gender")
      .agg(avg("age").as("averageAge"))
      .as[GenderWithAverageAge]
      .map(genderWithAge => GenderWithAverageAge(genderWithAge.gender, genderWithAge.averageAge.round))
      .show()
  }
}
